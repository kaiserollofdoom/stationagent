#!/bin/sh
git clone https://aur.archlinux.org/yay.git /tmp/yay
(cd /tmp/yay && makepkg -si)

sudo pacman -S ansible --needed

ansible-playbook -i "localhost," -c local stationagent.yml --ask-sudo-pass -vvvvvS
